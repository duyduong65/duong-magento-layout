<?php
namespace Personal\Information\Controller\Returnjson;

use Magento\Cms\Block\Block;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
    {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = [
            'image' => 'http://localhost/m0001/pub/static/version1581319179'.
            '/frontend/Magento/luma_US/Personal_Information/anh_ca_nhan.jpg',
            ' Full name' => 'Lê Duy Dương',
            'Age' => 23,
            'DateOfBirth' => '06/06/1997',
            'Description' => 'Vui tính dễ gần hoà đồng (chưa kể đẹp trai ngoan hiền và vẫn còn FA)'
        ];
        $result = $this->resultJsonFactory->create()->setData($data);
        return $result;
    }
}
