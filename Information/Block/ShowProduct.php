<?php

namespace Personal\Information\Block;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\View\Element\Template;

class ShowProduct extends \Magento\Framework\View\Element\Template
{
    protected $_product;

    public function __construct(
        Template\Context $context,
        array $data = [],
        CollectionFactory $productCollectionFactory
    ) {
        $this->_product = $productCollectionFactory;
        parent::__construct($context, $data);
    }

    public function getProducts()
    {
        $collection = $this->_product->create();
        $collection->addAttributeToFilter('feature', array('like' => '1'));
        $collection->setPageSize(10);
        return $collection;
    }
}
